#include <iostream>
#include <string>
#include "TransWorker.h"

using namespace std;

int main(int argc, char *argv[])
{
    //----------------------------------------------------------------------------//
    cout << "+--------------------------------------------------------------+" << endl;
    cout << "|  Translate HFSS project file to local FEM input file format  |" << endl;
    cout << "|               Author : Wang  Yong  2021.11.15                |" << endl;
    cout << "+--------------------------------------------------------------+" << endl;
    if (argc < 3) {
        cout << "Error: Not input HFSS project file or mesh file." << endl;
        return -1;
    }

    TransWorker worker;
    string sAedt, sMesh;
    bool bOk = worker.fnParseCmdArgu(argc, argv, sAedt, sMesh);
    if (!bOk) return -1;

    bOk = worker.fnLoadAedtFile(sAedt);
    if (!bOk) {
        cout << "Error: Load HFSS aedt file failed." << endl;
        return 1;
    }

    bOk = worker.fnLoadMeshFile(sMesh);
    if (!bOk) {
        cout << "Error: Load ngmesh file failed." << endl;
        return 2;
    }

    bOk = worker.fnGetPortCoord();
    if (!bOk) {
        cout << "Error: Extract port coordinate failed." << endl;
        return 3;
    }

    cout << "Info: output mesh in .msh file." << endl;
    bOk = worker.fnOutputMsh(sAedt);

    cout << "Info: output mesh in .tet file." << endl;
    bOk = worker.fnOutputTet(sAedt);

    cout << "Info: output project parameter in .xml file." << endl;
    bOk = worker.fnOutputXml(sAedt);

    return 0;
}
